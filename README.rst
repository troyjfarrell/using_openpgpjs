Using OpenPGP.js
================

This is a demonstration of how you can use OpenPGP.js_ to encrypt messages with
JavaScript.

.. _OpenPGP.js: http://openpgpjs.org/

